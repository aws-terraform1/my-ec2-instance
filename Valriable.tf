# Variable 
variable "ec2_count" {
  description = "number of count provisioning for ec2"
  default     = "1"
}

variable "ami_id" {
  description = "AMI for provisioning for ec2"
  default = "ami-098f16afa9edf40be"
}

variable "instance_type" {
  description = "type of ec2 for provisioning"
  default     = "t2.micro"
}

variable "subnet_id" {
  description = "subnet for provisioning of ec2"
  default = "subnet-0ac119d5187a39ba2"
}

variable "ec2_name" {
  description = "tag name of ec2"
  default = "test_gitlab_2"
}

variable "creator_id" {
 default = "Suraj"
}

variable "region_id" {
 default = "us-east-1"
}

variable "auto_id" {
 default = "opt-in"
}
variable "account_id" {
default = "457947379238"
}
variable "key_pair" {
default = "suraj_key3"
}