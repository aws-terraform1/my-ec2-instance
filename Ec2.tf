

resource "aws_instance" "Test_Instance5" {
  ami           = var.ami_id
  instance_type = var.instance_type
  subnet_id     = var.subnet_id
  key_name      = var.key_pair
  
    tags = {
    Name              = var.ec2_name
    creator           = var.creator_id
    region            = var.region_id
    automation        = var.auto_id
    account           = var.account_id

 }
    # depends_on      = [aws_key_pair.Lalit]
}